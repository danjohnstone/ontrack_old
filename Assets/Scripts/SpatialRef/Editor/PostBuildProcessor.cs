﻿using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

// ReSharper disable once UnusedMember.Global
public class PostBuildProcessor : IPostprocessBuild {
    public int callbackOrder {
        get { return 0; }
    }

    public void OnPostprocessBuild(BuildTarget target, string path) {
        string src = Path.Combine(Application.dataPath, "../proj.dll");
        // ReSharper disable once AssignNullToNotNullAttribute
        string dest = Path.Combine(Path.GetDirectoryName(path), "proj.dll");
        File.Copy(src, dest, true);
        Debug.Log("Copied file: " + src + " ==> " + dest);
        src = Path.Combine(Application.dataPath, "../nad/");
        // ReSharper disable once AssignNullToNotNullAttribute
        dest = Path.Combine(Path.GetDirectoryName(path), "nad/");
        CopyDirectory(src, dest, true);
        Debug.Log("Copied directory: " + src + " ==> " + dest);
    }

    private static void CopyDirectory(string sourceDirName, string destDirName, bool copySubDirs) {
        // Get the subdirectories for the specified directory.
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);

        if (!dir.Exists) {
            throw new DirectoryNotFoundException(
                "Source directory does not exist or could not be found: "
                + sourceDirName);
        }

        DirectoryInfo[] dirs = dir.GetDirectories();
        // If the destination directory doesn't exist, create it.
        if (!Directory.Exists(destDirName)) {
            Directory.CreateDirectory(destDirName);
        }

        // Get the files in the directory and copy them to the new location.
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files) {
            string temppath = Path.Combine(destDirName, file.Name);
            file.CopyTo(temppath, false);
        }

        // If copying subdirectories, copy them and their contents to new location.
        if (copySubDirs) {
            foreach (DirectoryInfo subdir in dirs) {
                string temppath = Path.Combine(destDirName, subdir.Name);
                CopyDirectory(subdir.FullName, temppath, true);
            }
        }
    }
}