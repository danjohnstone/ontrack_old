﻿using System;
using System.Reflection;
using UnityEngine;
using Object = UnityEngine.Object;

public class RuntimeEditableManager : MonoBehaviour {
    private void Awake() {
        Object[] obj = FindObjectsOfType(typeof(GameObject));
        foreach (Object o in obj) {
            GameObject g = (GameObject) o;
            Component[] components = g.GetComponents(typeof(Component));

            foreach (Component component in components) {
                Type type = component.GetType();
                FieldInfo[] mi = type.GetFields();

                foreach (FieldInfo memberInfo in mi) {
                    Attribute[] attrs = Attribute.GetCustomAttributes(memberInfo, typeof(RuntimeEditable));
                    foreach (Attribute attr in attrs) {
                        RuntimeEditable e = (RuntimeEditable) attr;

                        Debug.Log(g.name + "\n" + component.name + "\n" + e.DisplayName + "\n" + memberInfo.Name + "\n"
                                  + memberInfo.GetValue(component));

                        // TODO: Cool stuff here.
                        if (memberInfo.FieldType == typeof(float)) {
//                            memberInfo.SetValue(component, 23.0f);
                        }
                    }
                }
            }
        }
    }
}