﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using ClicksAndLinks.SpatialRef;
using UnityEngine;
using UnityEngine.Networking;

// 224597ee-aa55-4073-a139-0ca8905462fc

public class SensorManager : MonoBehaviour {
    public Datum Origin;
    public GameObject SensorPrefab;
    public string APIKey;

    private Queue<string> _sensorFeedsToQuery;
    private bool _feedReady;
    private string _b64APIKey;

    private static readonly string[] _feedIds = {
        "12225026-06e6-4435-82f1-33bf1c1ff1f3",
        "a2cf2c59-62e4-4064-b033-aad44cea3bb5",
        "5c516d02-ff44-41e6-9214-9aad96c718f1",
        "a0d58869-7e14-46c4-8fc0-4d0ef785fd1b",
        "b49d50c8-6866-4a53-bcf3-ac50613b1b67",
        "a48d985a-70a8-4f66-bb8c-7434a8de63fa",
        "d91a90be-6ad0-4968-8403-9eed672bb11a",
        "d2a352a0-db74-4ba9-90fa-e5c0f7cda2f4",
        "9c199582-94f1-433c-bf6e-f5eb7bac21d1",
        "b772ffbe-f2ac-44ad-bb4a-b35049acc632",
        "e349b78d-5cea-4547-a944-5d5af86e9d8e",
        "912251e3-ed16-4694-9729-d6464435ca4f",
        "5b0f9d70-1d3f-4a50-a627-b8199d7625dc",
        "c44cce1a-e6ec-4f9f-b6dc-0d0a98259eed",
        "f4642961-5dad-4afc-9b55-c89a9d75a32b",
        "f74f05b4-f722-4837-aa6a-4d8f03994796",
        "76c84520-d1c9-4734-8bda-42c0791dcd5e",
        "217cf8fb-7df0-4d17-853a-d01d488ce85c",
        "6d809e0b-5999-452d-887f-cf7c565c205c",
        "166c5c8a-3bfe-4f23-ae75-741c1f56401a",
        "b1e8d28e-4bef-48a7-bbc1-0fce7c114718",
        "2f23a263-81bf-40d8-9240-c216ec4464e6",
        "6a6f035c-82ab-4c2e-aa44-f3d27fbfb73e",
        "9d252a2b-5617-43f5-be47-8e5842c43e3a",
        "fdf4d56d-c480-4436-8aef-3179602f02fc",
        "2caa25d8-603d-4b10-821e-b2be956b24d2"
    };
    
    private void Awake() {
        byte[] toEncodeAsBytes = System.Text.Encoding.UTF8.GetBytes(APIKey + ":");
        _b64APIKey = Convert.ToBase64String(toEncodeAsBytes);
        _feedReady = true;
        _sensorFeedsToQuery = new Queue<string>();

        foreach (string feedId in _feedIds) {
            _sensorFeedsToQuery.Enqueue(feedId);
        }

//        StartCoroutine(getSensorCollection());
//        StartCoroutine(getSensorData("c44cce1a-e6ec-4f9f-b6dc-0d0a98259eed"));
    }

//    private IEnumerator getSensorCollection() {
//        UnityWebRequest uwr =
//            UnityWebRequest.Get("http://api.bt-hypercat.com/sensors/collections/5f8b7fb0-69e5-4f54-ab8b-cfb5779f1317");
//        uwr.SetRequestHeader("Authorization", "Basic " + _b64APIKey);
//
//        yield return uwr.SendWebRequest();
//
//        if (uwr.error != null) {
//            Debug.LogError(uwr.error);
//        } else {
//            XElement sensorCollection = XElement.Parse(uwr.downloadHandler.text);
//
//            try {
//                IEnumerable<XElement> sensorCollectionMembers = from members in sensorCollection.Descendants("member")
//                    select members;
//
//                foreach (XElement member in sensorCollectionMembers) {
////                    Debug.Log(member.Value);
//                    _sensorFeedsToQuery.Enqueue(member.Value);
////                    StartCoroutine(getSensorData(member.Value));
//                    _feedReady = true;
//                }
//            } catch (NullReferenceException) {
//                Debug.LogWarning("Unable to find any sensors");
//            }
//        }
//    }

    private IEnumerator getSensorData(string id) {
        UnityWebRequest uwr =
            UnityWebRequest.Get("http://api.bt-hypercat.com/sensors/feeds/" + id);
        uwr.SetRequestHeader("Authorization", "Basic " + _b64APIKey);

        yield return uwr.SendWebRequest();

        _feedReady = true;

        if (uwr.error != null) {
            Debug.LogError("http://api.bt-hypercat.com/sensors/feeds/" + id + "\n" + uwr.error);
        } else {
            XElement sensor = XElement.Parse(uwr.downloadHandler.text);

            try {
                IEnumerable<XElement> sensorLocation = from location in sensor.Descendants("location")
                    select location;

                var loc = sensorLocation.First();
                double lon = double.Parse(loc.Elements("lon").First().Value);
                double lat = double.Parse(loc.Elements("lat").First().Value);
                string name = sensor.Element("title").Value.Trim();

//                Debug.Log("http://api.bt-hypercat.com/sensors/feeds/" + id);
//                Debug.Log(lon + ", " + lat);
                Vector3d world = new Vector3d(lon, lat, 0.0);
                Vector3 worldToUnity = Origin.WorldToUnity(world);
//                Debug.Log(name + ": " + worldToUnity.magnitude + "m");
                GameObject sensorObject = Instantiate(SensorPrefab);
                sensorObject.transform.position = worldToUnity;
                sensorObject.name = name;
                sensorObject.transform.parent = transform;
            } catch (NullReferenceException) {
                Debug.LogWarning("Unable to find sensor location");
            }
        }
    }

    private void Update() {
        if (_feedReady && _sensorFeedsToQuery.Count > 0) {
            string feedId = _sensorFeedsToQuery.Dequeue();
            _feedReady = false;
            StartCoroutine(getSensorData(feedId));
        }
    }
}
