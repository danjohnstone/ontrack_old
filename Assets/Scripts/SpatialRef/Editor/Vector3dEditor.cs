﻿using UnityEditor;
using UnityEngine;

namespace ClicksAndLinks.Utility {
    [CustomEditor(typeof(Vector3d))]
    public class Vector3dEditor : Editor {
        private SerializedProperty _XProp;
        private SerializedProperty _YProp;
        private SerializedProperty _ZProp;

        private void OnEnable() {
            _XProp = serializedObject.FindProperty("x");
            _YProp = serializedObject.FindProperty("y");
            _ZProp = serializedObject.FindProperty("z");
        }

        public override void OnInspectorGUI() {
            serializedObject.Update();
            EditorGUILayout.DoubleField("Flangex", _XProp.doubleValue);
            EditorGUILayout.DoubleField("Flangey", _YProp.doubleValue);
            EditorGUILayout.DoubleField("Flangez", _ZProp.doubleValue);
            Debug.Log(_XProp);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
