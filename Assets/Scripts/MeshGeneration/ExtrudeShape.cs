﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtrudeShape{

    //defining the 2D shape to extrude 
    public Vector2[] verts;
    public Vector2[] normals;
    public float[] uChords;
    public int[] lines = new int[]
    {
        0,1,
        2,3,
        3,4,
        4,5

        // _
        //  \  
        //   \
        //    \
    };
}
