﻿[System.AttributeUsage(System.AttributeTargets.Field)]
public class RuntimeEditable : System.Attribute {
    private readonly string _displayName;
    
    public string DisplayName {
        get { return _displayName; }
    }

    public RuntimeEditable(string displayName) {
        _displayName = displayName;
    }
}
