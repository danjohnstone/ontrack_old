﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

    private Transform Cam;

	// Use this for initialization
	void Start () {
        Cam = Camera.main.transform;
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(Cam);
	}
}
