﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using ClicksAndLinks.SpatialRef;
using System.Linq;
using UnityEngine.UI;

public class JsonReader : MonoBehaviour {


    private string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "\\ECM1.json";

    public List<Tracks> trackData = new List<Tracks>();

    public List<Assets> assetData = new List<Assets>();

    public Datum Origin;

    private List<Vector3[]> _tracks;

    public GameObject TrackChunk;

    public GameObject Signal;

    public GameObject SpeedLimit;

    public GameObject Platform;

    public GameObject PlatConnector;

    public GameObject Point;

    public GameObject Hazard;

    public Sprite[] accesspointSprites;

    public GameObject TunnelPoint;

    public Transform TrackParent;

    public Transform SignalParent;

    public Transform SpeedLimitParent;

    public Transform PlatformParent;

    public Transform PointParent;

    public Transform HazardParent;

    public Transform TunnelPointParent;

    public Material rev;

    public Material forw;

    public Material Bi;

    public Material PlatMat;

    public float HeightOffset = 0.005f;

    private struct AssData
    {
        public string label;
        public Vector3 location;
    }

    private List<AssData> _assetNames;
    

    void Awake()
    {
        // reads ontrack json
        string json = File.ReadAllText(path);
        // coverts imported string into class
        ImportData jsonresult = JsonUtility.FromJson<ImportData>(json);
        // adds all tracks from imported string to track list
        for (int i = 0; i < jsonresult.ECM1.tracks.Length; i++)
        {
            trackData.Add(jsonresult.ECM1.tracks[i]);
        }
        // adds all assets to from imported string to assetlist
        for (int i = 0; i < jsonresult.ECM1.assets.Length; i++)
        {
            assetData.Add(jsonresult.ECM1.assets[i]);
        }
        // sets origin of datum script to the location of the third track
        Origin.Origin.x = trackData[0].locations[0].location.lng;
        Origin.Origin.y = trackData[0].locations[0].location.lat;
        Origin.Origin.z = 0.0f;
        // makes new vector 3 list the same length of track data list
        _tracks = new List<Vector3[]>(trackData.Count);
    }

    private void Start()
    {
        // loop goes through all the tracks in track data 
        foreach (Tracks track in trackData)
        {
            // new vector 3 array the same length as the track location
            Vector3[] t = new Vector3[track.locations.Length];
            int i = 0;
            // loop goes through each of the locations in track.locations
            foreach (Locations loc in track.locations)
            {
                // p is a vector 3 of gps to unity then added to the t array
                Vector3 p = Origin.GPSToUnity(new Vector3d(loc.location.lng, loc.location.lat, 0.0));
                t[i++] = p;
            }
            // adds t array to _tracks
            _tracks.Add(t);
        }
        // loops goes through _tracks 
        for (int i = 0; i < _tracks.Count;i++)
        {
            GameObject currentLine = Instantiate(TrackChunk, TrackParent);
            LineRenderer currentRendere = currentLine.GetComponent<LineRenderer>();
            // sets matrial based on track direction
            switch (trackData[i].direction)
            {
                case -1:
                    currentRendere.material = rev;
                    break;
                case 0:
                    currentRendere.material = Bi;
                    break;
                case 1:
                    currentRendere.material = forw;
                    break;
            }
            // sets length of line renderer positions array to correct length
            currentRendere.positionCount = _tracks[i].Length;
            // loops through line renderer positions array and sets track positions
            for (int y = 0; y < _tracks[i].Length;y++)
            {
                currentRendere.SetPosition(y, _tracks[i][y]);
            }
        }
        AssetPlacement();
    }

    private void AssetPlacement()
    {
        var plats = assetData.Where(pl => pl.type == "platform");
        _assetNames = new List<AssData>(plats.Count());
        foreach (Assets plat in plats)
        {
            AssignAssetPosition(plat, "platform", _assetNames);
        }

        var sigs = assetData.Where(s => s.type == "sig_controlled");
        _assetNames = new List<AssData>(sigs.Count());

        foreach (Assets sig in sigs)
        {
            AssignAssetPosition(sig, "sig_controlled", _assetNames);
        }

        var speedLims = assetData.Where(sl => sl.type == "speed_limit");
        _assetNames = new List<AssData>(speedLims.Count());

        foreach (Assets speedLim in speedLims)
        {
            AssignAssetPosition(speedLim, "speed_limit", _assetNames);
        }

        var points = assetData.Where(po => po.type == "point");
        foreach (Assets point in points)
        {
            AssignAssetPosition(point, "point", _assetNames);
        }

        var hazards = assetData.Where(ha => ha.type == "hazard");
        foreach (Assets hazard in hazards)
        {
            AssignAssetPosition(hazard, "hazard", _assetNames);
        }

        var tunnels = assetData.Where(tu => tu.type == "structure");
        foreach (Assets tunnel in tunnels)
        {
            AssignAssetPosition(tunnel, "structure", _assetNames);
        }    
    }

    private Vector3 PositionFromTrackMiles(int trackId, float milage , string locatedBy , int position)
    {
        Vector3 p;
        // gets the track assosiated with the asset
        Tracks track = trackData.Where(tr => tr.trackId == trackId).First();
        int foo = 0;
        int bar = 0;
        // switch to determin where the asset is located in relation to the track
        switch (locatedBy)
        {
            case "TRACK":
                for (int y = 0; y < track.locations.Length; ++y)
                {
                    if (milage <= track.locations[y].mileage)
                    {
                        foo = y;
                        break;
                    }
                    bar = y;
                }
                break;
            case "ELR":
                Debug.Log("Located by elr");
                break;
            default:
                Debug.Log("Asset location not in switch asset location is " + locatedBy);
                break;
        }

        float t = (milage - track.locations[bar].mileage) / (track.locations[foo].mileage - track.locations[bar].mileage);
        Vector3 p1 = Origin.GPSToUnity(new Vector3d(track.locations[foo].location.lng, track.locations[foo].location.lat, 0.0));
        Vector3 p2 = Origin.GPSToUnity(new Vector3d(track.locations[bar].location.lng, track.locations[bar].location.lat, 0.0));
        p = Vector3.Lerp(p2, p1, t) + Vector3.right * (position- 1) * HeightOffset;
        if (position == 1)
        {
            p += Vector3.up * HeightOffset;
        }

        return p;
    }

    private void AssignAssetPosition(Assets ass, string assetType, List<AssData> assDataList)
    {
        Vector3 p;
        // checks to see if assets tracks array is null
        bool plat = false;
        if (ass.location.tracks != null)
        {
            p = PositionFromTrackMiles(ass.location.tracks[0].trackId, ass.location.startMileage, ass.locatedBy, ass.location.position);
        }
        else
        {
            p = Origin.GPSToUnity(new Vector3d(ass.location.lng, ass.location.lat, 0.0f));
        }

        // Current game object
        GameObject go;
        // switch to determine the type of asset to instansiate 
        switch (assetType)
        {
            case "sig_controlled":
                go = Instantiate(Signal, SignalParent);
                break;
            case "speed_limit":
                go = Instantiate(SpeedLimit, SpeedLimitParent);
                break;
            case "platform":
                go = Instantiate(Platform, PlatformParent);
                plat = true;
                break;
            case "point":
                go = Instantiate(Point, PointParent);
                break;
            case "hazard":
                // switch to determine type of hazard
                switch (ass.assetGroup)
                {
                    case "Access Points":
                        go = Instantiate(Hazard, HazardParent);

                        switch (ass.subType)
                        {
                            case "MAR":
                                go.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Image>().sprite = accesspointSprites[0];
                                break;
                            case "MAV":
                                go.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Image>().sprite = accesspointSprites[0];
                                break;
                            case "MAP":
                                go.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Image>().sprite = accesspointSprites[1];
                                break;
                            case "MA":
                                go.transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<Image>().sprite = accesspointSprites[0];
                                break;
                            default:
                                Debug.Log("Sub type not found in switch");
                                break;
                        }
                        break;
                    case "Hazards":
                        go = Instantiate(Hazard, HazardParent);
                        break;
                    default:
                        Debug.Log("Assetgroup not found in switch asset gorup is " + ass.assetGroup);
                        go = Instantiate(Hazard, HazardParent);
                        break;
                }
                break;
            case "structure":
                go = Instantiate(TunnelPoint, TunnelPointParent);
                break;

            default:
                go = Instantiate(Signal, SignalParent);
                Debug.Log("Asset type not found in switch asset type = " + assetType);
                break;
        }
        // sets current game object position to position previously calculatd
        go.transform.position = p;
        // calls check beneath to see if there are any already instantiated objects below the current one
        CheckBeneath(p, go);
        AssData sd = new AssData();
        sd.label = ass.label;
        sd.location = p;
        if (sd.label == "")
        {
            if (ass.freeText == "")
            {
                sd.label = ass.freeText;
                ass.label = ass.freeText;
            }
            else
            {
                sd.label = "PLATFORM";
                ass.label = "PLATFORM";
            }
        }
        go.GetComponentInChildren<Text>().text = "<b>" + ass.label + "</b>";
        _assetNames.Append(sd);

        if (plat)
        {
            makePlatform(ass,go.transform, ass.location.endMileage);
        }
    }

    private void CheckBeneath(Vector3 pos, GameObject go)
    {
        //deactivates the collider of the gameobject we are currently dealing with
        go.GetComponentInChildren<Collider>().enabled = false;
        RaycastHit hit;
        // fires a ray downward to see if any colliders are benethe the ray if there are moves the instantiation position up a bit from hit position
        if (Physics.Raycast(pos += (Vector3.up * 10), go.transform.TransformDirection(Vector3.down), out hit, 10))
        {
                float z = hit.point.z;
                go.transform.position += (Vector3.up * ((10 - hit.distance) + HeightOffset));
        }
        // turns the collider of the game object we are dealing with back on;
        go.GetComponentInChildren<Collider>().enabled = true;
    }

    private void makePlatform(Assets ass, Transform trans ,float endMile)
    {
        Vector3 fwd /*= trans.position + trans.forward  * mileToMeter(endMile)*/;
        GameObject go;
        List<Vector3> platvecs = new List<Vector3>();
        Tracks track = trackData.Where(tr => tr.trackId == ass.location.tracks[0].trackId).First();

        go = Instantiate(Platform,PlatformParent);
        fwd = PositionFromTrackMiles(ass.location.tracks[0].trackId, (float)ass.location.tracks[0].endMiles, ass.locatedBy, ass.location.position);
        go.transform.position = fwd;
        go.GetComponentInChildren<Text>().text = "<b>" + "Plat End" + "</b>";

        for (int i = 0; i < track.locations.Length; i++)
        {
            if (track.locations[i].mileage >= ass.location.tracks[0].startMiles && track.locations[i].mileage <= ass.location.tracks[0].endMiles)
            {
                Vector3 p = Origin.GPSToUnity(new Vector3d(track.locations[i].location.lng, track.locations[i].location.lat, 0.0f));
                if (ass.location.position == 1)
                {
                    p += Vector3.up * HeightOffset;
                }
                else if (ass.location.position == 0)
                {
                    p += Vector3.left * HeightOffset;
                }
                else if(ass.location.position == 1)
                {

                }

                platvecs.Add(p);
            }
        }

        GameObject ngo = Instantiate(PlatConnector, PlatformParent);
        ngo.name = "PlatConnector";
        ngo.AddComponent<LineRenderer>();
        LineRenderer lr = ngo.GetComponent<LineRenderer>();
        lr.positionCount = platvecs.Count +1;

        int y = 0;
        for (int i = 0;i< lr.positionCount;i++)
        {
            if (i == 0)
            {
                lr.SetPosition(0, trans.position);
            }
            else if (i == (lr.positionCount -1))
            {
                lr.SetPosition(i, go.transform.position);
            }
            else
            {
                lr.SetPosition(i, platvecs[i]);
                y = i;
            }
        }
        CheckBeneath(fwd,go);
    }

    //converts miles to meters
    private float mileToMeter(float mile)
    {
        return ((mile * 1609.344f)/(float)Origin.Scale);
    }
    // convets chains to meters
    private float chainToMeter(float chain)
    {
        return ((chain * 20.117f)/(float)Origin.Scale);
    }
}

[System.Serializable]
public struct ImportData
{
    public ecm1 ECM1;
}

[System.Serializable]
public struct ecm1
{
    public Tracks[] tracks;
    public Assets[] assets;
}

[System.Serializable]
public struct Tracks
{
    public int id;
    public int trackId;
    public string name;
    public int direction;
    public Locations[] locations;
}

[System.Serializable]
public struct Location
{
    public double lat;
    public double lng;
}

[System.Serializable]
public struct Locations
{
    public float elrMileage;
    public float mileage;
    public Location location;
    public int junctionId;
}

[System.Serializable]
public struct Assets
{
    public int id;
    public string type;
    public string subType;
    public string assetGroup;
    public string locatedBy;
    public string behaviour;
    public string label;
    public string freeText;
    public a_location location;
}

[System.Serializable]
public struct a_tracks
{
    public int assetId;
    public double endMiles;
    public int id;
    public double startMiles;
    public int trackId;
}

[System.Serializable]
public struct a_location
{
    public float startMileage;
    public float endMileage;
    public a_tracks[] tracks;
    public int position;
    public double lat;
    public double lng;
}