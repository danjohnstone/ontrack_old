﻿using UnityEngine;

namespace ClicksAndLinks.Logger {
    /// <summary>
    /// A helper class that wraps <see cref="ClicksAndLinks.Logger.EventLogHandler"/>.
    /// </summary>
    public class EventLog {
        /// <summary>
        /// Write an <b>information</b> message to the Event Log and the Unity console.
        /// </summary>
        /// <param name="tag">A string to put before the message</param>
        /// <param name="message">The message that is written to the log</param>
        public static void Info(string tag, string message) {
            EventLogHandler unused = EventLogHandler.Instance;
            Debug.unityLogger.Log(tag, message);
        }

        /// <summary>
        /// Write a <b>warning</b> message to the Event Log and the Unity console.
        /// </summary>
        /// <param name="tag">A string to put before the message</param>
        /// <param name="message">The message that is written to the log</param>
        public static void Warning(string tag, string message) {
            EventLogHandler unused = EventLogHandler.Instance;
            Debug.unityLogger.LogWarning(tag, message);
        }

        /// <summary>
        /// Write an <b>error</b> message to the Event Log and the Unity console.
        /// </summary>
        /// <param name="tag">A string to put before the message</param>
        /// <param name="message">The message that is written to the log</param>
        public static void Error(string tag, string message) {
            EventLogHandler unused = EventLogHandler.Instance;
            Debug.unityLogger.LogError(tag, message);
        }
    }
}
