﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ToggleMenuOpen : MonoBehaviour {

    public GameObject Menu;

    public Text btnText;

    public GameObject SLP;

    public GameObject PLP;

    public GameObject SIP;

    public GameObject POP;

    public GameObject HAP;

    public GameObject TUP;

    public Button[] btns;

    public Sprite img;

    public int NumTog;

    public void toggleMenu()
    {
        Menu.SetActive(!Menu.activeSelf);
        if (Menu.activeSelf == true)
        {
            btnText.text = "-";
        }
        else
        {
            btnText.text = "+";
        }
    }

    public void togglePlatforms()
    {
        PLP.SetActive(!PLP.activeSelf);
        CheckToggles(PLP, 0);
    }

    public void togglePoints()
    {
        POP.SetActive(!POP.activeSelf);
        CheckToggles(POP, 1);
    }

    public void toggleSignals()
    {
        SIP.SetActive(!SIP.activeSelf);
        CheckToggles(SIP, 2);
    }

    public void toggleSpeedLimits()
    {
        SLP.SetActive(!SLP.activeSelf);
        CheckToggles(SLP, 3);
    }

    public void toggleHazards()
    {
        HAP.SetActive(!HAP.activeSelf);
        CheckToggles(HAP, 4);
    }

    public void toggleTunnels()
    {
        TUP.SetActive(!TUP.activeSelf);
        CheckToggles(TUP,5);
    }

    public void CheckToggles(GameObject Check , int btn)
    {
        if (Check.activeSelf)
        {
            btns[btn].image.sprite = img;
        }
        else
        {
            btns[btn].image.sprite = null;
        }
    }
}
