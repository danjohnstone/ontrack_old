﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using ProjApi;

namespace ClicksAndLinks.SpatialRef {
    public class UndefinedSRSException : Exception {
        public UndefinedSRSException() {
        }

        public UndefinedSRSException(string message) : base(message) {
        }
    }

    public class UnknownUnitsException : Exception {
        public UnknownUnitsException(string message) : base(message) {
        }
    }

    public class Datum : MonoBehaviour {
        public Vector3d Origin;
        public double Scale = 1.0;

        public string ProjectionDef {
            get { return _projectionDef; }
            set { _projectionDef = value; }
        }

        [SerializeField] private string _projectionDef;
        private Projection _SRS;
        private Projection _UTMProjection;
        private Projection _WGS84;
        private Vector3d _UTMOrigin;
        private double _toMetres = 1.0;

        private void Awake() {
            if (_projectionDef == "") {
                throw new UndefinedSRSException("Undefined SRS definition.");
            }

            string src = Path.Combine(Application.dataPath, "../nad/");
            Proj.pj_set_searchpath(1, new[] {src});

            try {
                _SRS = new Projection(_projectionDef);
            } catch (Exception) {
                throw new UndefinedSRSException("Cannot find SRS with definition: \"" + _projectionDef + "\"");
            }
            
            _WGS84 = new Projection("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0");

            // If the chosen SRS is a geographic one, then we use the UTM grid to translate to and from metres.
            if (_SRS.IsLatLong) {
                string proj = FindUTMZone(Origin);
                try {
                    _UTMProjection = new Projection("+init=" + proj);
                } catch (ArgumentException) {
                    throw new UndefinedSRSException("Cannot find UTM zone: \"" + proj + "\" for origin: " + Origin);
                }

                double[] xd = {Origin.x};
                double[] yd = {Origin.y};
                double[] zd = {Origin.z};
                Projection.Transform(_SRS, _UTMProjection, xd, yd, zd);

                _UTMOrigin = new Vector3d(xd[0], yd[0], zd[0]);
            } else {
                // A few SRSs use units other than metres. Handle those odd units as best we can.
                Regex r = new Regex(@"\+to_meter=(\S+)");
                Match m = r.Match(_SRS.Definition);

                if (m.Length > 0) {
                    _toMetres = double.Parse(m.Groups[1].Value);
                    return;
                }

                r = new Regex(@"\+units=(\S+)");
                m = r.Match(_SRS.Definition);

                if (m.Length == 0) {
                    throw new UnknownUnitsException("Unknown units of length. I am assuming metres, " +
                                                    "but this is almost certainly wrong.");
                }

                string units = m.Groups[1].Value.ToLower();

                switch (units) {
                    case "m":
                        _toMetres = 1.0;
                        break;

                    case "ft":
                        _toMetres = 0.3048;
                        break;

                    case "us-ft":
                        _toMetres = 0.304800609601219;
                        break;

                    case "lnk":
                    case "link":
                        _toMetres = 0.201168;
                        break;

                    case "chain":
                        _toMetres = 20.1168;
                        break;

                    case "km":
                        _toMetres = 1000.0;
                        break;

                    default:
                        throw new UnknownUnitsException("Unknown unit of length: \"" + units +
                                                        "\" I am assuming metres, but this is almost certainly wrong.");
                }
            }
        }

        private void Start() {
//            Vector3 p = new Vector3(-34.28427f, 0.0f, 32.28427f);
//            Vector3d unityToWorld = UnityToWorld(p);
//            Vector3 worldToUnity = WorldToUnity(unityToWorld);
//            Debug.Log(p.ToString("F6"));
//            Debug.Log(unityToWorld);
//            Debug.Log(worldToUnity.ToString("F6"));
        }

        public Vector3d UnityToWorld(Vector3 pos) {
            if (_SRS == null) {
                throw new UndefinedSRSException();
            }

            pos = transform.InverseTransformPoint(pos);

            // Swizzle the coordinates so +ve Z is up.
            Vector3d dpos = new Vector3d(pos.x, pos.z, pos.y);

            dpos *= Scale;

            if (_SRS.IsLatLong) {
                dpos += _UTMOrigin;
                double[] xd = {dpos.x};
                double[] yd = {dpos.y};
                double[] zd = {dpos.z};
                Projection.Transform(_UTMProjection, _SRS, xd, yd, zd);
                dpos = new Vector3d(xd[0], yd[0], zd[0]);
            } else {
                dpos /= _toMetres;
                dpos += Origin;
            }

            return dpos;
        }

        public Vector3 WorldToUnity(Vector3d pos) {
            if (_SRS == null) {
                throw new UndefinedSRSException();
            }

            if (_SRS.IsLatLong) {
                double[] xd = {pos.x};
                double[] yd = {pos.y};
                double[] zd = {pos.z};
                Projection.Transform(_SRS, _UTMProjection, xd, yd, zd);
                pos = new Vector3d(xd[0], yd[0], zd[0]);
                pos -= _UTMOrigin;
            } else {
                pos -= Origin;
                pos *= _toMetres;
            }

            pos /= Scale;

            Vector3 fpos = new Vector3((float) pos.x, (float) pos.z, (float) pos.y);

            return transform.TransformPoint(fpos);
        }

        public Vector3d UnityToGPS(Vector3 pos) {
            Vector3d dpos = UnityToWorld(pos);

            double[] xd = {dpos.x};
            double[] yd = {dpos.y};
            double[] zd = {dpos.z};
            Projection.Transform(_SRS, _WGS84, xd, yd, zd);
            dpos = new Vector3d(xd[0], yd[0], zd[0]);

            return dpos;
        }

        public Vector3 GPSToUnity(Vector3d pos) {
            double[] xd = {pos.x};
            double[] yd = {pos.y};
            double[] zd = {pos.z};
            Projection.Transform(_WGS84, _SRS, xd, yd, zd);
            pos = new Vector3d(xd[0], yd[0], zd[0]);

            Vector3 fpos = WorldToUnity(pos);

            return fpos;
        }

        private static string FindUTMZone(Vector3d longLatAlt) {
            int zoneNumber = Mathd.FloorToInt((longLatAlt.x + 180.0) / 6.0) + 1;

            if (longLatAlt.y >= 56.0 && longLatAlt.y < 64.0 && longLatAlt.x >= 3.0 && longLatAlt.x < 12.0) {
                zoneNumber = 32;
            }

            // Special zones for Svalbard
            if (longLatAlt.y >= 72.0 && longLatAlt.y < 84.0) {
                if (longLatAlt.x >= 0.0 && longLatAlt.x < 9.0) {
                    zoneNumber = 31;
                } else if (longLatAlt.x >= 9.0 && longLatAlt.x < 21.0) {
                    zoneNumber = 33;
                } else if (longLatAlt.x >= 21.0 && longLatAlt.x < 33.0) {
                    zoneNumber = 35;
                } else if (longLatAlt.x >= 33.0 && longLatAlt.x < 42.0) {
                    zoneNumber = 37;
                }
            }

            if (longLatAlt.y <= 0.0) {
                zoneNumber += 100;
            }

            zoneNumber += 32600;

            return "epsg:" + zoneNumber;
        }
    }
}