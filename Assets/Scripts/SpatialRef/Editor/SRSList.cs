﻿using System.Collections.Generic;
using UnityEditor;

namespace ClicksAndLinks.SpatialRef {
    [CustomEditor(typeof(Datum))]
    public class SRSList : Editor {
        private List<string> _projectionDefs;
        private List<string> _projectionNames;

        private void Awake() {
            _projectionNames = new List<string>();
            _projectionDefs = new List<string>();

            _projectionNames.Add("WGS84");
            _projectionDefs.Add("+init=epsg:4326");
            _projectionNames.Add("Web Mercator");
            _projectionDefs.Add("+init=epsg:5327");
            _projectionNames.Add("OSGB36");
            _projectionDefs.Add("+init=epsg:27700");
            _projectionNames.Add("Broken");
            _projectionDefs.Add("+init=epsg:3427700");
        }

        public override void OnInspectorGUI() {
            DrawDefaultInspector();
            Datum datum = (Datum) target;
            int index = 0;
            
            if (_projectionDefs.Contains(datum.ProjectionDef)) {
                index = _projectionDefs.IndexOf(datum.ProjectionDef);
            }

            index = EditorGUILayout.Popup("Projection", index, _projectionNames.ToArray());
            datum.ProjectionDef = _projectionDefs[index];
            EditorUtility.SetDirty(target);
        }
    }
}