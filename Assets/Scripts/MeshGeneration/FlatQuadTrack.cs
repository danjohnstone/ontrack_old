﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatQuadTrack : MonoBehaviour {

    public LineRenderer LR;

    public Vector3[] Points;

    public GameObject Point;

    public GameObject PointRep;

    public float trackWidth;

    private GameObject previousNode;

    private float distanceBetweenNode;

	// Use this for initialization
	void Start () {
        LR.positionCount = Points.Length;
        for (int i = 0; i<Points.Length;i++)
        {

            GameObject currentNode = Instantiate(Point);
            currentNode.transform.position = Points[i];
            LR.SetPosition(i, Points[i]);

            if (i!=0)
            {
                previousNode = currentNode;
            }
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
