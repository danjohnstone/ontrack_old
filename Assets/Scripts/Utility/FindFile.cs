﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ClicksAndLinks.Utility {
	public class FindFile {
		private readonly List<string> _paths;

		private static FindFile _instance;

		/// <summary>
		/// Gets or creates the singleton instance of this class.
		/// </summary>
		public static FindFile Instance {
			get {
				if (_instance == null) {
					_instance = new FindFile();
				}

				return _instance;
			}
		}

		private FindFile() {
			string appSuffix = Path.Combine(Application.companyName, Application.productName);

			List<string> paths = new List<string> {
		        ".",
		        Application.dataPath,
		        Application.persistentDataPath,
		        Application.streamingAssetsPath,
		        Application.temporaryCachePath,
#if !UNITY_WSA_10_0
			    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), appSuffix),
			    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), appSuffix),
		        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), appSuffix),
				Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), appSuffix)
#endif
		    };

		    // Some of the special folders can be duplicates on some systems.
		    _paths = paths.Distinct().ToList();
		}

	    public string Find(string fileName) {
	        foreach (string path in _paths) {
	            string filePath = Path.Combine(path, fileName);
	            if (File.Exists(filePath)) {
	                return filePath;
	            }
	        }

	        throw new FileNotFoundException("Unable to find file", fileName);
	    }

	    public override string ToString() {
	        return "\"" + string.Join("\", \"", _paths.ToArray()) + "\"";
	    }
	}
}