﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

public class BasicMeshGenerator : MonoBehaviour {

    private Vector3[] Verts = new Vector3[]
    {
        new Vector3(1,0,1),
        new Vector3(-1,0,1),
        new Vector3(1,0,-1),
        new Vector3(-1,0,-1)
    };

    private Vector3[] Normals = new Vector3[]
    {
        new Vector3(0,1,0),
        new Vector3(0,1,0),
        new Vector3(0,1,0),
        new Vector3(0,1,0)
    };

    private Vector2[] uvs = new Vector2[]
    {
        new Vector2(0,1),
        new Vector2(0,0),
        new Vector2(1,1),
        new Vector2(1,0)
    };

    private int[] tris = new int[]
    {
        0,2,3,
        3,1,0
    };
    private MeshFilter meshFilter;
    private MeshRenderer meshRendere;
    private Mesh mesh;
    private Material mat;

    void Start () {
        mat = new Material(Shader.Find("Standard"));
        gameObject.GetComponent<Renderer>().material = mat;
        meshFilter = GetComponent<MeshFilter>();
        if (meshFilter.sharedMesh == null)
        {
            meshFilter.sharedMesh = new Mesh();
            meshFilter.sharedMesh.name = "SWAG";
        }
        mesh = meshFilter.sharedMesh;

        mesh.Clear();
        mesh.vertices = Verts;
        mesh.normals = Normals;
        mesh.uv = uvs;
        mesh.triangles = tris;

	}
	
	void Update () {
		
	}
}
